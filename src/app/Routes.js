import React from 'react'

// external components
import CssBaseline from '@material-ui/core/CssBaseline';
import Container from '@material-ui/core/Container'

// pages
import Home from './pages/home'
import About from './pages/about'
import HowWorks from './pages/howWorks'
import SignUp from './pages/signUp'
import WhichIs from './pages/whichIs'
import Projects from './pages/projects'

// components
import Header from './components/header'
import background from './components/svgs/mobile.png'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'


function Routes() {
  return (
    <div className={styles.gradientColor}>
    <CssBaseline />
    <Container disableGutters={true} maxWidth="lg">
    <Header/>
      <Router>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
          <Route path="/how-it-works" component={HowWorks} />
          <Route path="/signup" component={SignUp} />
          <Route path="/which-is" component={WhichIs} />
          <Route path="/projects" component={Projects} />
        </Switch>
      </Router>
    </Container>
    </div>
  );
}

const styles = {
  backgroundContainer: {
    backgroundImage: `url(${background})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    height: '100vh',
    }
}

export default Routes;
