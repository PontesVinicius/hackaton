import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import { Link } from 'react-router-dom'
// import Button from '@material-ui/core/Button'
import MenuSVG from '../svgs/menu.svg'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem'
import { IconButton } from '@material-ui/core'

import 'typeface-roboto'

const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    appBar: {
        color: "white",
        backgroundColor: '#1d1d2c',
        height: '104px'
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
      fontFamily: 'Roboto',
      fontWeight: '800',
      marginLeft: '8%',
    },
    subtitle: {
        fontSize: '11px',
        color: '#f1f37b',
        fontWeight: 'bold',
        fontStrech: 'normal',
        fontStyle: 'normal',
        lineHeight: '1',
        letterSpacing: '0.3px',
        textAlign: 'center',
        position: 'absolute',
        marginTop: '4.7%',
        marginLeft: '17%'
        
    },
    menuSVG: {
        marginTop: '25%',
        marginRight: theme.spacing(3)
    },

  }));


function Header() {
    const [anchorEl, setAnchorEl] = React.useState(null)
    const classes = useStyles();

    const handleClick = event => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(null)
    }

    const goTo = location => {
        window.location.href = location
        }

    return (
        <>
        <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
            style={{color: 'red'}}
        >
          
          <MenuItem onClick={() => goTo('/about')}>Quem somos</MenuItem>
          <MenuItem onClick={() => goTo('/which-is')}>O que é</MenuItem>
          <MenuItem onClick={() => goTo('/how-it-works')}>Como funciona</MenuItem>
        </Menu>
        <div className={classes.root}>
            <AppBar className={classes.appBar} position="inherit">
                <Toolbar>
                    <Typography onClick={() => goTo('/')} variant="h6" className={classes.title}>
                        HACKATON
                    </Typography>
                    <span className={classes.subtitle}>05 DE MARÇO</span>
                    <IconButton aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick} edge="end" color="inherit" arial-label="menu">
                        <img src={MenuSVG} className={classes.menuSVG} />
                    </IconButton>
                </Toolbar>
            </AppBar>
        </div>
        </>
    )
}

export default Header