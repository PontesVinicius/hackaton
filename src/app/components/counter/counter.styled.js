import Styled from 'styled-components'


export const StyledDiv = Styled.div`
    text-align: center;
`;

export const StyledMoment = Styled.p`
    font-size: 29px;
    font-weight: 900;
    color: black;
    font-family: 'Roboto';
    margin-top: 20px;
`;

export const StyledDate = Styled.div`
    width: 70px;
    height: 55px;
    transform: skew(-15deg);
    background: white;
    border-radius: 3px;
    margin-left: 3%;
    margin-top: 20px;
    float: left;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 10px 8px 1px black;
`;