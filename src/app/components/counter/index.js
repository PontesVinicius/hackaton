import React from 'react'
import { 
    StyledDiv,
    StyledMoment, 
    StyledDate,
    StyledTextTitle,
    StyledTextDiv,
    StyledTextSpan,
    StyledSubText
} from './counter.styled'
import Typography from '@material-ui/core/Typography'

export default class Counter extends React.Component {
    constructor(props) {
    super(props)

    this.state = {
        days: '',
        hours: '',
        minutes: '',
        seconds: ''
    
    }

    }

    componentDidMount = () => {
        setInterval(() => {
            this.renderer()
        }, 1000)
    }

    renderer = () => {
        const countDownDate = new Date("Mar 5, 2020 23:59:59").getTime()
        const now = new Date().getTime()
        const distance = countDownDate - now
        const days = Math.floor(distance / (1000 * 60 * 60 * 24))
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60))
        const seconds = Math.floor((distance % (1000 * 60)) / 1000)
        this.setState({days, hours, minutes, seconds})
    }


    render() {
        const { days, hours, minutes, seconds } = this.state

        return (
            <StyledDiv>
                <StyledDate>
                    <StyledMoment>{days}</StyledMoment>
                </StyledDate>
                <div style={{position: 'absolute', color: 'black',marginLeft: '8.5%', marginTop: '14.9%', fontSize: '12px'}}>dias</div>
                <StyledDate>
                    <StyledMoment>{hours}</StyledMoment>
                </StyledDate>
                <div style={{position: 'absolute', color: 'black',marginLeft: '28.5%', marginTop: '14.9%', fontSize: '12px'}}>horas</div>
                <StyledDate>
                    <StyledMoment>{minutes}</StyledMoment>
                </StyledDate>
                <div style={{position: 'absolute', color: 'black',marginLeft: '47.8%', marginTop: '14.9%', fontSize: '12px'}}>minutos</div>
                <StyledDate>
                    <StyledMoment>{seconds}</StyledMoment>
                </StyledDate>
                <div style={{position: 'absolute', color: 'black',marginLeft: '68.5%', marginTop: '14.9%', fontSize: '12px'}}>segundos</div>
            </StyledDiv>
        )
    }
}
