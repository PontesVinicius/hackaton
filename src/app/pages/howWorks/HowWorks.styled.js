import Styled from 'styled-components'

export const StyledTitle = Styled.h1`
    color: #b68ef1;
`;

export const StyledText = Styled.p`
    color: white;
    font-size: 16px;
    font-style: normal;
    line-height: 2.19;
    text-align: left;
`