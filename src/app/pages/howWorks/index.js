import React from 'react'
import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles';

import {
    StyledTitle,
    StyledText
} from './HowWorks.styled'

const useStyles = makeStyles(() => ({
    Button1: {
        color: 'white',
        border: '2px solid #2ea265',
        width: '270px',
        height: '60px',
        borderRadius: 0,
        textTransform: 'capitalize',
        marginBottom: '30px',
    },
    divButton: {
        textAlign: 'center'
    }
}))

function HowWorks() {
    const classes = useStyles()
    return (
        <Container maxWidth="lg">
            <StyledTitle>Como funciona?</StyledTitle>
            <StyledText>
                O Hackathon Altran RJ será no dia 5 de março de 2020, 
                na Praia de Botafogo, 518 - 5º andar, Botafogo/ RJ. Serão 8 
                horas de desenvolvimento com diversos profissionais e estudantes. 
            </StyledText>
            <StyledTitle>Regras</StyledTitle>
            <StyledText>
                Os projetos deverão ser apresentados de maneira funcional e não apenas 
                um esboço/protótipo, uma vez que a estrutura lógica do código também será avaliada.
                É imprescindível levar o seu notebook. A gente cuida da infraestrutura, wifi e material gráfico ;)
                <br/>
                <br/>
                Esse evento é exclusivo para maiores de idade. 
            </StyledText>
            <div className={classes.divButton}>
            <Button size='large' className={classes.Button1} variant="outlined" >Inscreva-se >></Button>
            </div>
        </Container>
    )
}

export default HowWorks