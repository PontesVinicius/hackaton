import React from 'react'
import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles';

import {
    StyledTitle,
    StyledText
} from './WhichIs.styled'

const useStyles = makeStyles(() => ({
    Button1: {
        color: 'white',
        border: '2px solid #2ea265',
        width: '270px',
        height: '60px',
        borderRadius: 0,
        textTransform: 'capitalize',
        marginBottom: '30px',
    },
    divButton: {
        textAlign: 'center'
    }
}))

function WhichIs() {
    const classes = useStyles()
    return (
        <Container maxWidth="lg">
            <StyledTitle>O que é?</StyledTitle>
            <StyledText>Hackathon é um evento para reunir programadores e designers que desenvolvem softwares e querem colocar em prática uma ideia incrível por várias horas seguidas. É uma experiência única de conhecer pessoas legais, fazer networking, competir, aprender e colaborar. Não deixe de participar! Teremos muitas conversas, risadas, projetos, codificações e, para finalizar, um happy hour com muita pizza e chopp. Tudo isso por nossa conta ;)

            </StyledText>
            <div className={classes.divButton}>
            <Button size='large' className={classes.Button1} variant="outlined" >Inscreva-se >></Button>
            </div>
        </Container>
    )
}

export default WhichIs