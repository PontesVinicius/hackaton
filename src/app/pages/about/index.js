import React from 'react'
import Container from '@material-ui/core/Container'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles';

import {
    StyledTitle,
    StyledText
} from './about.styled'

const useStyles = makeStyles(() => ({
    Button1: {
        color: 'white',
        border: '2px solid #2ea265',
        width: '270px',
        height: '60px',
        borderRadius: 0,
        textTransform: 'capitalize',
        marginBottom: '30px',
    },
    divButton: {
        textAlign: 'center'
    }
}))

function About() {
    const classes = useStyles()
    return (
        <Container maxWidth="lg">
            <StyledTitle>Quem somos?</StyledTitle>
            <StyledText>
            Com 11 escritórios e mais 
de 1200 colaboradores no Brasil, trabalhamos com empresas líderes de mercado com o objetivo de criar novas experiencias para nossos clientes, implantar produtos de ponta e fornecer eficiências operacionais através da tecnologia. 
Somos especializados em engenharia de produto e manufatura, Industria 4.0, IOT, estratégia digital, Desenvolvimento de software, Cloud, Cybersercurity, API Management, Robotic Process Automation (RPA) e full service provider em desenvolvimento SAP e Salesforce.

A nível mundial, somos líder global em consultoria de engenharia e R&D, a Altran posiciona-se como uma empresa inovadora no desenvolvimento de produtos e serviços à medida das necessidades dos clientes. 
Neste sentido, a Altran acompanha os clientes em cada etapa do projeto, desde o planeamento estratégico à fase de produção. Há mais de 30 anos no mercado global.
            </StyledText>
            <div className={classes.divButton}>
            <Button size='large' className={classes.Button1} variant="outlined" >Inscreva-se >></Button>
            </div>
        </Container>
    )
}

export default About