import React from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';


const useStyles = makeStyles(() => ({
    Button1: {
        color: 'white',
        border: '2px solid #ff79c6',
        width: '270px',
        height: '60px',
        borderRadius: 0,
        textTransform: 'capitalize',
        marginBottom: '30px',
        marginTop: '20px'
    },
    Button2: {
       color: 'white',
       border: '2px solid #f1f37b',
       width: '270px',
       height: '60px',
       borderRadius: 0,
       textTransform: 'capitalize',
       marginBottom: '30px',
       marginTop: '20px'
    },
    divButton: {
        textAlign: 'center'
    },
    divMother: {
        marginTop: '8%',
        width: 'auto',
        height: '100%',
        float: 'left',
        border: '1px solid #565656',
        borderRight: 'none',
        borderTop: 'none',
        borderBottom: 'none',
        paddingLeft: '2%'
    },
    cadastreSeu: {
        color: '#4df276',
        fontSize: '25px'
    },
    seuProjeto: {
        color: '#ff79c6',
        fontSize: '18px',
        margin: 0,
        padding: 0,
    },
    followers: {
        color: '#b68ef1',
        fontSize: '14px',
        fontStyle: 'italic',
    },
    flashy: {
        color: 'white',
        fontSize: '16px',
        lineHeight: '1.5',
    },
    projectName: {
        color: '#f1f37b',
        fontSize: '18px',
        margin: '10px 0px 10px 0px',
        padding: 0,
    }
}))

let data = []
data = [
    {
        id: '1',
        project_name: 'Projeto X',
        project_followers: '21',
        project_description: ' Projeto show de bola que vai fazer sucesso pra caramba, toop demais'
    },
    {
        id: '2',
        project_name: 'Projeto Y',
        project_followers: '121',
        project_description: 'Projeto show de bola que vai fazer sucesso pra caramba, toop demais,Projeto show de bola que vai fazer sucesso pra caramba, toop demais, Projeto show de bola que vai fazer sucesso pra caramba, toop demais, Projeto show de bola que vai fazer sucesso pra caramba, toop demais'
    },
    {
        id: '3',
        project_name: 'Projeto Z',
        project_followers: '121',
        project_description: 'Projeto show de bola'
    }
]

const Projects = () => {
    const classes = useStyles()
    
    return (
        <Container maxWidth="lg">
            <div className={classes.divMother}>
                <span className={classes.cadastreSeu}>Cadastre seu</span>
                <div className={classes.divMother}>
                    <span className={classes.seuProjeto}>Cadastre seu projeto</span>
                    <br/>
                    <span className={classes.followers}>x inscritos</span>
                    <br/>
                    <br/>
                    <span className={classes.flashy}>
                    Tem uma ideia incrível? Já 
                    pensou nas pessoas da sua 
                    equipe? Clica no botão abaixo 
                    e se prepare pro evento!
                    </span>
                    <Button size='large' className={classes.Button1} variant="outlined" >Cadastre seu projeto >></Button>
                    <br/>
                    {data.map(i => (
                        <div>
                        <p className={classes.projectName}>{i.project_name}</p>
                        <span className={classes.followers}>{i.project_followers} inscritos</span>
                        <br/>
                        <span className={classes.flashy}>{i.project_description}</span>
                        <Button size='large' className={classes.Button2} variant="outlined" >Inscreva-se >></Button>
                        </div>
                    ))}

                </div>
            </div>
        </Container>
    )
}

export default Projects