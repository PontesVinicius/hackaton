import React from 'react'
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles';
import Checkbox from '@material-ui/core/Checkbox';

import {
    StyledRadio
} from './signup.styled'


const useStyles = makeStyles(() => ({
    divMother: {
        marginTop: '8%',
        width: 'auto',
        height: '100%',
        float: 'left',
        border: '1px solid #565656',
        borderRight: 'none',
        borderTop: 'none',
        borderBottom: 'none',
        paddingLeft: '2%'
    },
    signUpTitle: {
        color: '#4df276',
        fontSize: '28px'
    },
    personalInfo: {
        color: '#ff79c6',
        fontSize: '20px',
        margin: 0,
        padding: 0,
    },
    projectName: {
        color: '#f1f37b',
        fontSize: '18px',
        margin: '10px 0px 10px 0px',
        padding: 0,
    },
    inputForm: {
        marginTop: '10px',
        width: '300px',
        height: '42px',
        border: '1px solid #6272a4',
        backgroundColor: 'transparent',
        color: '#969090',
        fontSize: '18px',
        textAlign: 'left',
        paddingLeft: '10px'
    },
    followers: {
        color: '#b68ef1',
        fontSize: '14px',
        fontStyle: 'italic',
    },
    radioLabel: {
        color: 'white',
        fontSize: '20px',
        lineHeight: '1.35'
    },
    labelCheckbox: {
        color: 'white',
        fontSize: '19px'
    },
    checkbox: {
        width: '20px',
        height: '20px',
        border: '1px solid #6272a4',
        backgroundColor: 'red',
    }
}))


function Signup() {
    const classes = useStyles()

    return (
        <Container maxWidth='lg'>
            <div className={classes.divMother}>
                <span className={classes.signUpTitle}>Function.Inscrição</span>
                <div className={classes.divMother}>
                    <span className={classes.personalInfo}>.Informações pessoais</span>
                    <br/>
                    <br/>
                    <form>
                        <p className={classes.projectName}>Nome()</p>
                        <input className={classes.inputForm} type="text" placeholder={`"#nome"`} />
                        <p className={classes.projectName}>E-mail()</p>
                        <input className={classes.inputForm} type="text" placeholder={`"#e-mail"`} />
                        <p className={classes.projectName}>Data de nascimento</p>
                        <input className={classes.inputForm} type="text" placeholder={`"#dd/mm/aaaa"`} />
                        <p className={classes.projectName}>Instituição de Ensino</p>
                        <span className={classes.followers}>(Optional)</span>
                        <input className={classes.inputForm} type="text" placeholder={`"#instituição de ensino"`} />
                        <p className={classes.projectName}>Empresa / Organização</p>
                        <span className={classes.followers}>(Optional)</span>
                        <input className={classes.inputForm} type="text" placeholder={`"#empresa/organização"`} />
                        
                        <p className={classes.projectName}>Tamanho da camisa()</p>
                        <StyledRadio type="radio" id="tamanhoP" name="tamanho" value="p" />
                        <label className={classes.radioLabel} for="tamanhoP">"#P"</label><br></br>
                        <StyledRadio type="radio" id="tamanhoM" name="tamanho" value="p" />
                        <label className={classes.radioLabel} for="tamanhoM">"#M"</label><br></br>
                        <StyledRadio type="radio" id="tamanhoG" name="tamanho" value="p" />
                        <label className={classes.radioLabel} for="tamanhoG">"#G"</label><br></br>
                        <StyledRadio type="radio" id="tamanhoGG" name="tamanho" value="p" />
                        <label className={classes.radioLabel} for="tamanhoGG">"#GG"</label><br></br>
                    <br/>
                    <span className={classes.personalInfo}>.Outras Informações</span>
                    <br/>
                    <br/>
                    <p className={classes.projectName}>Tecnologias de atuação()</p>
                    <input className={classes.inputForm} type="text" placeholder={`"#tecnologias"`} />
                    
                    <p className={classes.projectName}>Objetivo(s) com o hackaton()</p>
                    <input className={classes.checkbox} type="checkbox" id="itent1" name="itent1" value="itent1"/>
                    <label className={classes.labelCheckbox}for="itent1">"#experimentação de ideias novas"</label><br/>
                    <input className={classes.checkbox} type="checkbox" id="itent2" name="itent2" value="itent2"/>
                    <label className={classes.labelCheckbox}for="itent2">"#aprender / colaborar"</label><br/>
                    <input className={classes.checkbox} type="checkbox" id="itent3" name="itent3" value="itent3"/>
                    <label className={classes.labelCheckbox}for="itent3">"#oportunidade de trabalho"</label><br/>
                    <input className={classes.checkbox} type="checkbox" id="itent4" name="itent4" value="itent4"/>
                    <label className={classes.labelCheckbox}for="itent4">"#networking"</label><br/>
                    <input className={classes.checkbox} type="checkbox" id="itent5" name="itent5" value="itent5"/>
                    <label className={classes.labelCheckbox}for="itent5">"#outros"</label><br/>

                    <p className={classes.projectName}>Você autoriza o uso da sua imagem no evento?()</p>
                        <StyledRadio type="radio" id="sim" name="sim" value="sim" />
                        <label className={classes.radioLabel} for="sim">"#Sim"</label><br></br>
                        <StyledRadio type="radio" id="nao" name="nao" value="nao" />
                        <label className={classes.radioLabel} for="nao">"#Não"</label><br></br>

                        <p className={classes.projectName}>Você concorda com o termo de confidencialidade sobre as ideias e códigos gerados no hackathon?()</p>
                        <StyledRadio type="radio" id="sim" name="sim" value="sim" />
                        <label className={classes.radioLabel} for="sim">"#Sim"</label><br></br>
                        <StyledRadio type="radio" id="nao" name="nao" value="nao" />
                        <label className={classes.radioLabel} for="nao">"#Não"</label><br></br>
                </form>
                </div>
            </div>
        </Container>
    )
}

export default Signup