import Styled from 'styled-components'

export const StyledRadio = Styled.input`
    opacity: .2;
    width: 24px;
    heigth: 24px;
`