import React from 'react'
import background from '../../components/svgs/mobile.png'
import { Link } from 'react-router-dom'
import Counter from '../../components/counter'
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'
// import CssBaseline from '@material-ui/core/CssBaseline'
import {
StyledTextDiv,
StyledTextTitle,
StyledTextSpan,
StyledTextDate,
StyledDescText,
StyledDot,
StyledArrow,
StyledDivArrow,
StyledDivTextFull,
TextFull,
StyledButtonDiv,
StyledLocationDiv,
StyledMapDiv
} from './home.styled'
import Container from '@material-ui/core/Container'

const useStyles = makeStyles(theme => ({
    Button1: {
        color: 'white',
        border: '2px solid #1abfd9',
        width: '270px',
        height: '60px',
        borderRadius: 0,
    },
    Button2: {
        marginTop: '27px',
        marginBottom: '35px',
        color: 'white',
        border: '2px solid #edf881',
        width: '270px',
        height: '60px',
        borderRadius: 0,

    },
    placeIcon: {
        color: '#ff79c6',
    }
}))




function Home() {
    const classes = useStyles()

    return (
        <>
        <Container style={styles.backgroundContainer} maxWidth="lg">
            <Counter />
            <StyledTextDiv>
                <StyledTextTitle>
                    HA-CKA-TON
                    <StyledTextSpan>altran <span style={{color: '#edf881'}}>RJ</span></StyledTextSpan>
                    <StyledTextDate>05 DE MARÇO</StyledTextDate>
                    <StyledDescText>
                        CODIFIQUE<StyledDot>.</StyledDot>APRENDA
                        <br/>COLABORE<StyledDot>.</StyledDot>INOVE
                    </StyledDescText>
                    <StyledDivArrow>
                        <StyledArrow>
                        </StyledArrow>
                    </StyledDivArrow>
                </StyledTextTitle>
            </StyledTextDiv>
            <StyledDivTextFull>
                <TextFull>
                    Você é programador apaixonado pelas stacks ReactJS, NodeJS, Android - Kotlin, IOS - Swift
                    ou UX Designer? Este é o seu momento! Venha pensar fora da caixa, criar soluções inovadores
                    e desafiadoras em projetos web, app mobile ou app SmartTV! Teremos brindes, prêmio para o
                    projeto vencedor e uma oportunidade de trabalhar com a gente.
                    Incrivel, né?
                </TextFull>
            </StyledDivTextFull>
            <StyledButtonDiv>
                <Link to='/projects'><Button size='large' className={classes.Button1} variant="outlined" >Conheça os projetos</Button></Link>
                <Button size='large' className={classes.Button2} variant="outlined">Inscreva-se Já >></Button>
            </StyledButtonDiv>
            <StyledLocationDiv>
                Praia de Botafogo, 518 - 5º andar<br/>
                Botafogo / RJ
            </StyledLocationDiv>
            <StyledMapDiv>
                <Icon className={classes.placeIcon}>place</Icon>Ver mapa
            </StyledMapDiv>
        </Container>
        </>
    )
}

const styles = {
    backgroundContainer: {
      backgroundImage: `url(${background})`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      height: '100vh',
      }
  }


export default Home
