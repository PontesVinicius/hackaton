import Styled from 'styled-components'

export const StyledTextTitle = Styled.p`
    font-family: 'Passion One', cursive;
    align-text: center;
    color: white;
    font-weight: 500;
    font-size: 130px;
    letter-spacing: 2px;
    font-strech: normal;
    line-height: 1;
`;

export const StyledTextDiv = Styled.div`
    margin-top: -25%;
    float: left;
    text-align: justify;
    margin-left: 15%;
`;

export const StyledTextSpan = Styled.span`
    font-family: 'Baloo Bhai', cursive;
    color: white;
    display: block;
    font-size: 49px;
    margin-left: 4%;
`;

export const StyledDescText = Styled.div`
    margin-top: 8%;
    font-size: 25px;
    white-space: pre;
    text-align: center;
    margin-left: -20%;
`

export const StyledTextDate = Styled.div`
    margin-top: 4%;
    font-size: 37px;
    font-weight: 500;
    margin-left: 3%;
    text-align: justify;
    color: #f1f37b;
`;

export const StyledDot = Styled.span`
    color: #f1f37b;    
`;

export const StyledDivArrow = Styled.div`
    margin-top: -22%;
    margin-left: 35%;
`

export const StyledArrow = Styled.div`
    margin-top: 52%;
    box-sizing: border-box;
    height: 8vw;
    width: 8vw;
    border-style: solid;
    border-color: white;
    border-width: 0px 5px 5px 0px;
    transform: rotate(45deg);
`;

export const StyledDivTextFull =  Styled.div`
    margin-top: -35%;
    float: left;
    width: 100%;
    height: 450px;
    text-align: center;
`;

export const TextFull = Styled.p`
    color: white;
    font-size: 20px;
    line-height: 1.8;
`;

export const StyledButtonDiv = Styled.div`
    float: left;
    text-align: center;
`

export const StyledLocationDiv = Styled.div`
    color: white;
    text-align: center;
    font-size: 17px;
    
`

export const StyledMapDiv = Styled.div`
    margin-top: 10%;
    text-align: center;
    color: #ff79c6;
    font-size: 20px;
    text-decoration: underline;
`
