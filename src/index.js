import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './app/Routes';
import './index.css'

ReactDOM.render(<Routes />, document.getElementById('root'));

